<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     * @return void
     */
    public function run() {
        $default_fields = [
            'name' => 'Default User',
            'email' => 'admin@demo.com',
            'password' => Hash::make('123456'),
            'email_verified_at' => date('Y-m-d h:i:s')
        ];

        User::updateOrCreate(['email' => 'admin@demo.com'], $default_fields);
    }
}
