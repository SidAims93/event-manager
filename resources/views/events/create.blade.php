@extends('layouts.master')
@section('content')
    @php 
        $flag = isset($event) && $event !== null;
    @endphp
    <div class="container">
        <div class="row mt-1">
            <div class="col-md-12">
                <form method="POST" action="@if($flag) {{route('events.update', $event->id)}} @else {{route('events.store')}} @endif">
                    @csrf
                    @if($flag)
                        @method('PUT')
                        <input type="hidden" name="id" value="{{$event->id}}" />
                    @endif  
                    <div class="form-group">
                        <label for="eventName">Name</label>
                        <input type="text" class="form-control" name="name" id="eventName" @if($flag) value="{{$event->name}}" @else value="{{old('name')}}" @endif placeholder="Any event Name e.g. Rock And Roll Concert">
                        @error('name')
                        <div class="alert alert-danger mt-1">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="eventDesc">Description</label>
                        <textarea class="form-control" name="description" id="eventDesc" rows="3">@if($flag){{$event->description}}@endif</textarea>
                        @error('description')
                        <div class="alert alert-danger mt-1">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="eventDate">Date</label>
                        <input class="form-control" type="date" name="date" @if($flag) value="{{$event->date}}" @else value="{{old('date')}}" @endif>
                        @error('date')
                        <div class="alert alert-danger mt-1">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="eventLocation">Location</label>
                        <select class="form-control" id="eventLocation" name="location_id">
                            <option value="">--SELECT--</option>
                            @foreach($locations as $location)
                                <option value="{{$location->id}}" @if($flag && $location->id === $event->location_id) selected @endif>
                                    {{$location->name}}
                                </option>
                            @endforeach
                        </select>
                        @error('location_id')
                        <div class="alert alert-danger mt-1">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input class="btn btn-md btn-success" type="submit" @if($flag) value="Update" @else value="Create" @endif>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection