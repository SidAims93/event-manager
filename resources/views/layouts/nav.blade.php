<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('events.index')}}">{{config('app.name')}}</a>
  <a class="navbar-brand" href="{{route('locations.index')}}">Manage Locations</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      {{-- <li class="nav-item">
        <a class="nav-link" href="#">Manage Events</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Manage Locations</a>
      </li> --}}
    </ul>
    @include('layouts.searchBar')
  </div>
</nav>