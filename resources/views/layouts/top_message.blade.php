@if(Session::has('success'))
    <div class="alert alert-success text-center">
        <h4>{{Session::get('success')}} {{Session::forget('success')}}</h4>
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger text-center">
        <h4>{{Session::get('error')}} {{Session::forget('error')}}</h4>
    </div>
@endif