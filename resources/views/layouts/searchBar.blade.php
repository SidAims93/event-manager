@php 
    $routeName = Route::currentRouteName();
@endphp
@if($routeName !== null && Str::contains($routeName, 'event'))
<form class="form-inline my-2 my-lg-0" action="" method="GET">
    <input class="form-control mr-sm-2" name="searchTerm" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
</form>
@endif