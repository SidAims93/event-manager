<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.head')
    <body>
        @if(Auth::check())
            @include('layouts.nav')
            @include('layouts.top_message')
        @endif
        @yield('content')
    </body>
    @include('layouts.js')
</html>