@extends('layouts.master')
@section('content')
    @php 
        $flag = isset($location) && $location !== null;
    @endphp
    <div class="container">
        <div class="row mt-1">
            <div class="col-md-12">
                <form method="POST" action="@if($flag) {{route('locations.update', $location->id)}} @else {{route('locations.store')}} @endif">
                    @csrf
                    @if($flag)
                        @method('PUT')
                        <input type="hidden" name="id" value="{{$location->id}}" />
                    @endif  
                    <div class="form-group">
                        <label for="locationName">Name</label>
                        <input type="text" class="form-control" name="name" id="locationName" @if($flag) value="{{$location->name}}" @else value="{{old('name')}}" @endif placeholder="Any Location Name e.g. Punjab">
                        @error('name')
                        <div class="alert alert-danger mt-1">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="locationDesc">Description</label>
                        <textarea class="form-control" name="description" id="locationDesc" rows="3">@if($flag){{$location->description}}@endif</textarea>
                        @error('description')
                        <div class="alert alert-danger mt-1">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input class="btn btn-md btn-success" type="submit" @if($flag) value="Update" @else value="Create" @endif>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection