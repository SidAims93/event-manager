<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {
    protected $guarded = [];

    public function getEvents() {
        return $this->belongsToMany(Event::class, 'events', 'location_id');
    }
}
