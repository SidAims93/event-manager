<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {
    
    protected $guarded = [];

    public function getLocation() {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }
}
