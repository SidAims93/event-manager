<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationCreate;
use App\Http\Requests\LocationUpdate;
use App\Models\Location;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

class LocationsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('locations.list', ['locations' => Location::get()]);
    }

    public function create() {
        try {
            return view('locations.create');
        } catch(Exception $e) {
            Log::info($e->getMessage().' '.$e->getLine());
            return view('locations.create');
        }
    }

    public function store(LocationCreate $request) {
        try {
            $request = $request->validated();
            Location::create($request);
            return back()->with('success', 'Location added successfully !');
        } catch(Exception $e) {
            Log::info($e->getLine().' '.$e->getMessage());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        } catch(Throwable $e) {
            Log::info($e->getLine().' '.$e->getMessage());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        }
    }

    public function destroy($id) {
        try {
            Location::where('id', $id)->delete();
            return back()->with('success', 'Location Deleted !');
        } catch(Exception $e) {
            Log::info($e->getLine().' '.$e->getMessage());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        }
    }

    public function show($id) {
        try {
            $location = Location::where('id', $id)->first();
            return view('locations.create', ['location' => $location]);
        } catch(Exception $e) { 
            Log::info($e->getLine().' '.$e->getMessage());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        }
    }

    public function update(LocationUpdate $request) {
        try {
            $request = $request->validated();
            Location::where('id', $request['id'])->update($request);
            return back()->with('success', 'Location updated successfully !');
        } catch(Exception $e) {
            Log::info($e->getLine().' '.$e->getMessage());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        } catch(Throwable $e) {
            Log::info($e->getLine().' '.$e->getMessage());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        }
    }
}
