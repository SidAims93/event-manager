<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEvent;
use App\Http\Requests\UpdateEvent;
use App\Models\Event;
use App\Models\Location;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EventsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index(Request $request) {
        $events = $this->getEvents($request->all());
        return view('welcome', ['events' => $events]);
    }

    //In case there is some search parameter passed, we can filter it out from database.
    public function getEvents($request) {
        try {
            return Event::whereHas('getLocation')->where(function ($query) use ($request) {
                        if(isset($request['searchTerm']))
                            return $query->where('name', 'LIKE', '%'.$request['searchTerm'].'%');
                    })->get();
        } catch(Exception $e) {
            Log::info($e->getMessage().' '.$e->getLine());
            return null;
        }
    }

    private function getLocations() {
        return Location::get();
    }

    public function create() {
        try {
            return $this->atLeastOneLocationExists() ? view('events.create', ['locations' => $this->getLocations()]) : redirect()->route('locations.create')->with('error', 'To Create An Event There Must Be A Location !');
        } catch(Exception $e) {
            Log::info($e->getMessage().' '.$e->getLine());
            return view('events.create');
        }
    }

    //This method tells you if at least one location is present in the db. 
    //If not then you need to create a location first.
    private function atLeastOneLocationExists() {
        try {
            return Location::count() > 0;
        } catch(Exception $e) {
            Log::info($e->getMessage().' '.$e->getLine());
            return true;
        }
    }

    public function destroy($id) {
        try {
            Event::where('id', $id)->delete();
            return back()->with('success', 'Event Deleted !');
        } catch(Exception $e) {
            return redirect()->route('events.index')->with('error', $e->getMessage());
        }
    }

    public function show($id) {
        try {
            $event = Event::where('id', $id)->first();
            return view('events.create', ['event' => $event, 'locations' => $this->getLocations()]);
        } catch(Exception $e) {
            return redirect()->route('events.index')->with('error', $e->getMessage());
        }
    }

    public function store(CreateEvent $request) {
        try {
            $request = $request->validated();
            Event::updateOrCreate(['name' => $request['name']], $request);
            return redirect()->route('events.index')->with('success', 'Event Created !');
        } catch(Exception $e) {
            Log::info($e->getMessage().' '.$e->getLine());
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        }
    }

    public function update(UpdateEvent $request) {
        try {
            $request = $request->validated();
            Event::where('id', $request['id'])->update($request);
            return back()->with('success', 'Event Updated !');
        } catch(Exception $e) {
            return back()->with('error', $e->getMessage().' '.$e->getLine());
        }
    }
}
