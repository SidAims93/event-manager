<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LocationUpdate extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules() {
        return [
            'id' => 'required',
            'name' => 'required|unique:locations,name,'. request()->get('id'),
            'description' => 'required'
        ];
    }

    public function messages() {
        return [
            'id.required' => 'ID is required !',
            'name.required' => 'Name is required to update !',
            'description.required' => 'Description is required !'
        ];
    }
}
