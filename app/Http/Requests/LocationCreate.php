<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LocationCreate extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|unique:locations,name',
            'description' => 'required'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Please specify a location name',
            'name.unique' => 'A location with this name already exists. Please choose another.',
            'description.required' => 'Please specify description for the location'
        ];
    }
}
